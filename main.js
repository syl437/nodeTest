
//import * as express from 'express';
const express = require('express');
const hbs = require('hbs');
const cors = require('cors')
const bodyParser = require('body-parser');
const port = process.env.PORT || 3000;
var app = express();
app.use(bodyParser.json());

var corsOptions = {
  origin: '*',
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204 
}

app.use(cors(corsOptions))

//app.set('view engine','hbs')
//app.use(express.static(__dirname+'/public'));

app.route('/api').post((req, res) => {
  res.send({name:'shay',age:"39"});
});

app.get('/',(req,res)=>{
    //res.send('Hello Express !!');
    //res.send('<h1>Hello Express !!</h1>');
    res.send({name:'shay',age:"39"});
})

app.get('/about',(req,res)=>{
    //res.send('Hello Express !!');
    //res.send('<h1>Hello Express !!</h1>');
    res.send("11");
})

app.get('/about1',(req,res)=>{
    //res.send('Hello Express !!');
    //res.send('<h1>Hello Express !!</h1>');
    res.render('about.hbs',
    {
        pageTitle:'About Page 12' ,
        currentYear : new Date().getFullYear(),
    });
})

app.get('/bad',(req,res)=>{
    res.send({errorMessage:'Unable to handle reques'});
})

app.listen(port,()=>{
    console.log("server Up" , port);
});